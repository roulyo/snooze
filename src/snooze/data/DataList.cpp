
    #include <forge/Project.h>
    #include "DataList.h"

    namespace DataList
    {

    namespace Entity
    {

        constexpr forge::DataNameId PlayableCharacter = forge::DataNameId("Entity::PlayableCharacter");

    }

    namespace Sprite
    {

        constexpr forge::DataNameId ForgeSprite = forge::DataNameId("Sprite::ForgeSprite");

    }

    namespace Texture
    {

        constexpr forge::DataNameId ForgeSheet = forge::DataNameId("Texture::ForgeSheet");

    }

    }
