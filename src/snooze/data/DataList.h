
    #pragma once
    #include <forge/engine/data/Data.h>

    namespace DataList
    {

    namespace Entity
    {

        extern const forge::DataNameId PlayableCharacter;

    }

    namespace Sprite
    {

        extern const forge::DataNameId ForgeSprite;

    }

    namespace Texture
    {

        extern const forge::DataNameId ForgeSheet;

    }

    }
